console.log('hello js')
"use strict"

// ------------------------------------------------

const btnAudio = document.querySelector(".btn-listen");
const btnStop = document.querySelector(".btn-stop");
const titleHeadphone = document.querySelector(".headphone");
const titleListening = document.querySelector(".listening");
const storyDes = document.querySelector(".story-desc");

btnAudio.addEventListener('click', ()=>{
    btnAudio.classList.toggle('on');
    btnStop.classList.toggle('on');
    titleHeadphone.classList.toggle('on');
    titleListening.classList.toggle('on');
    storyDes.classList.toggle('on');
})

btnStop.addEventListener('click', ()=>{
    btnAudio.classList.remove('on');
    btnStop.classList.remove('on');
    titleHeadphone.classList.remove('on');
    titleListening.classList.remove('on');
    storyDes.classList.remove('on');
})

// installed 된 socket.io 를 socket 변수에 지정 
const socket = io(); 
console.log(socket);

// ------------------------------------------------------------------
//1. emit 기능을 통해 'chatting' 이라는 채널에 'from front' 라는 메시지를 전송. 
socket.emit("chatting", "from front")


//3. chatting 채널에 반환된 값을 다시 console 에 띄움. 
socket.on("chatting", (data) => {
    console.log(data)
})


console.log(socket)


