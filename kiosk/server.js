// Server 구축 

const express = require("express")
const http = require("http")
const app = express(); 
const path = require("path")
const server = http.createServer(app); 

const socketIO = require("socket.io")

const io = socketIO(server); 

// index, js 파일이 들어 있는 폴더 지정, app.js 를 기준으로  
app.use(express.static(path.join(__dirname, "src")))

// Server Port Number 
const PORT = process.env.PORT || 5000; 





// 2. 서버에 접속하면 socket 실행. 첫째, chatting 채널에서 받은 data(from front)를 console log 에 띄우고 이것을 다시 chatting 채널에 그래 반가워 + 원래 받은 data 를 io 에 다시 data 로 반환 

io.on("connection", (socket) => {
   socket.on("chatting", (data) => {
       console.log(data)
       io.emit("chatting", data)

   })
})

// chatting 

server.listen(PORT, () => console.log(`server is running ${PORT}`))

